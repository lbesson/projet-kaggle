#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" A Quadratic Discriminant Analysis (QDA)

The doc is here : http://scikit-learn.org/dev/modules/generated/sklearn.qda.QDA.html#sklearn.qda.QDA

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

    $ python QDA.py

--------------------------------------------------------------------------------
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from KaggleModel import *

################################################################################
# Beginning to learn
from sklearn.qda import QDA
from sklearn.utils import shuffle

################################################################################
clf = QDA()
print("Learning...")
clf = clf.fit(train_data[0::,1::],train_data[0::,0])
#: The score for this classifier.
score = (100.0*clf.score(train_data[0::,1::], train_data[0::,0]) )
print(" Proportion of perfect fitting for the training dataset = %2.2f%%" % 
 score )
# ~ must be < 95%

print("Class priors (sum to 1) = %s." % clf.priors_)

for i in xrange(len(data_attributes) - 1):
	print("	For the attribute %s	, mean=%s." %
	 ( data_attributes[1+i], str(clf.means_[:,i]) ) )

# Cross Validation
from sklearn import cross_validation
bs = cross_validation.Bootstrap(number_passengers, n_iter = 5, train_size = 0.67)
for train_index, test_index in bs:
	clf = clf.fit(train_data[train_index,1::],train_data[train_index,0])
	score = (100.0*clf.score(train_data[test_index,1::], train_data[test_index,0]) )
	print(" Score for test part = %2.2f%%" % score )

# Predict on the testing set
test_file_object = csv.reader(open('test.csv', 'rb'))
header = test_file_object.next()

print("Predicting for the testing dataset")
Output = clf.predict(test_data) 

# Write the output
open_file_object = csv.writer(open("csv/QDA_best.csv", "wb"))

z = 0
for row in test_file_object:
 row.insert(0, int(Output[z])) # Insert the prediction at the start of the row
 open_file_object.writerow(row) # Write the row to the file
 z += 1

print("Prediction: wrote in the file csv/QDA_best.csv.")
