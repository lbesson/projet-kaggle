#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset, regarding the sibsp.

:Source: `<../../Plot_sibsp.py>`_

Histogramme
-----------
Cela montre la distribution des victimes et survivants selon
le nombre de frères & soeurs, et époux/épouses présents
parmis les autres passagers du Titanic.

.. image:: plots/passengers_bar_sibsp.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_bar_sibsp.*
   :align: center 


Histogramme (taux de survie)
----------------------------
Ce second montre le taux de survie (plutôt une fraction qu'un taux).

.. image:: plots/passengers_bar_sibsp2.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_bar_sibsp2.*
   :align: center 


On devrait voir que les passagers "un peu" accompagné survivent plus que ceux
seuls.

Diagramme
---------

.. image:: plots/passengers_pie_sibsp.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_pie_sibsp.*
   :align: center

Les deux catégories les plus présentes sont 0 et 1 (c'est-à-dire les passagers
voyageant seuls ou avec **UN** seul membre de leur famille).

Et ce diagramme montre que les personnes seules survivent moins (68% → 72%),
alors que les personnes accompagnées survivent plus (23% → 33%).
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics regarding the sibsp ...")
################################################################################
indexes_survived = data[0::, survived] == '1'
indexes_dead = data[0::, survived] == '0'

pylab.plot(data[indexes_survived, sibsp], 'bo')
pylab.plot(data[indexes_dead, sibsp], 'ro')
pylab.suptitle(u'Nombre de frères & soeurs, et époux/épouses des passagers (parmis les passagers)')
pylab.legend(('Survivants', 'Victimes'))
pylab.savefig("plots/passengers_scatter_sibsp.svg")
print("Ploting the passengers repartition on a scatter: plots/passengers_scatter_sibsp.svg")
pylab.draw()
pylab.clf()

################################################################################
x = [ i for i in xrange(1+int( data[0::, sibsp].astype(np.float).max() )) ]
x2 = [ i+0.5 for i in xrange(1+int( data[0::, sibsp].astype(np.float).max() )) ]
y0 = [ 0.0 for i in xrange(len(x)) ]
y1 = [ 0.0 for i in xrange(len(x)) ]
y2 = [ 0.0 for i in xrange(len(x)) ]

for i in xrange(number_passengers):
 y0[ int(data[i, sibsp].astype(np.float)) ] += 1
 if data[i, survived] == '1':
  y1[ int(data[i, sibsp].astype(np.float)) ] += 1
 else:
  y2[ int(data[i, sibsp].astype(np.float)) ] += 1

pylab.suptitle(u'Nombre de frères & soeurs, et époux/épouses des passagers (parmis les passagers)')

pylab.subplot(2,1,1)
pylab.bar(x, y0, color='black', edgecolor='white')
pylab.legend(['Passagers (total)'])
pylab.ylabel(u"Nombre de passagers")

pylab.subplot(2,1,2)
pylab.bar(x, y1, color='blue', edgecolor='white', width=0.4)
pylab.bar(x2, y2, color='red', edgecolor='white', width=0.4)
pylab.legend(['Survivants', 'Victimes'])
pylab.ylabel(u"Nombre de passagers")

pylab.savefig("plots/passengers_bar_sibsp.svg")
print("Ploting the passengers repartition on a bar: plots/passengers_bar_sibsp.svg")
pylab.draw()
pylab.clf()

################################################################################
yn = [ 0.0 for i in xrange(len(x)) ]
for i in xrange(len(yn)):
 if y0[i]>0:
   yn[i] = float(y1[i]) / float(y0[i])

pylab.suptitle(u'Nombre de frères & soeurs, et époux/épouses des passagers (parmis les passagers)')

pylab.subplot(2,1,1)
pylab.bar(x, y0, color='black', edgecolor='white')
pylab.legend(['Passagers (total)'])
pylab.ylabel(u"Nombre de passagers")

pylab.subplot(2,1,2)
pylab.bar(x, yn, color='green', edgecolor='white')
pylab.axis([min(x), 1+max(x),0.0,1.0])
pylab.ylabel(u"Taux de survie")

pylab.savefig("plots/passengers_bar_sibsp2.svg")
print("Ploting the passengers repartition on a bar: plots/passengers_bar_sibsp2.svg")
pylab.draw()
pylab.clf()

################################################################################
pylab.suptitle(u'Nombre de frères & soeurs, et époux/épouses des passagers (parmis les passagers)')

pylab.subplot(2,1,1)
pylab.pie(y0, autopct="%2.2f%%")
pylab.axis('equal')
pylab.title('Total')
pylab.legend( ['%s' % i for i in x] )

pylab.subplot(2,2,3)
pylab.pie(y1, autopct="%2.2f%%")
pylab.title('Survivants')
pylab.axis('equal')

pylab.subplot(2,2,4)
pylab.pie(y2, autopct="%2.2f%%")
pylab.title('Victimes')
pylab.axis('equal')

pylab.savefig("plots/passengers_pie_sibsp.svg")
print("Ploting the passengers repartition on a bar: plots/passengers_pie_sibsp.svg")
pylab.draw()
pylab.clf()
