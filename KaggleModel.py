#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Transformation des données.

Ce script permet de lire les données des deux fichiers
`train.csv <../../train.csv>`_, `test.csv <../../test.csv>`_
et de les transformer en matrics de flottants.

Conversion
----------
Les données lues depuis ces deux fichiers sont sous formes
de numpy.array de **strings**.
On doit les convertir en **float** pour apprendre.

Les deux difficultés sont pour le port d'embarcation et le genre.
On convertit : 'male' et 'female' en nombre (1 ou 0).

Seulement, pour ces attributs, le problème sous-jacent est de la
classification (un passager est un homme **OU** une femme),
pas de régression (un passager n'est pas 71% une femme...).

Malgré ça, le fait qu'il y a de nombreux autres attributs
fait qu'on pourra quand même passer nos données à notre algorithme
de classification,
sans que la présence de deux attributs "catégoricaux" et non numériques
ne le perturbe.

:Source: `doc de scikit-learn <http://scikit-learn.org/>`_

Complétion
----------
Le problème principal est le manque de certaines données.

Par exemple, 20% des passengers n'ont pas d'âge dans l'ensemble
d'apprentissage.

Et quelques passengers n'ont pas de "prix de tickets" dans l'ensemble
de test.

On completera donc simplement par la *valeur moyenne* de cet
attribut. Cette valeur est calculée dans le script `Kaggle <Kaggle.html>`_.
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *
from sklearn.utils import shuffle

################################################################################
#: Load in the csv file
print("Opening the file 'train.csv' and 'test.csv'...")
csv_file_object = csv.reader(open('train.csv', 'rb')) 
test_file_object = csv.reader(open('test.csv', 'rb'))

#: Skip the fist line as it is a header
header = csv_file_object.next()
header = test_file_object.next()

#: Table of translation: strings -> float, for the gender
table_sex = {'male':1, 'female':0}
#: Table of translation: strings -> float, for the port
table_embarked = {'C':0, 'S':1, 'Q':2}

# Building the train dataset
train_data = []
for row in csv_file_object:
    row_dict = {}
    row_dict[survived] = int(row[survived])
    for attr in [pclass, sibsp, parch]:
     row_dict[attr] = int(row[attr])

    row_dict[fare] = float(row[fare])/float(fare_max) if row[fare]!='' else fare_mean/fare_max
    row_dict[embarked] = table_embarked[row[embarked]] if row[embarked]!='' else table_embarked['S']
    row_dict[age] = float(row[age])/float(age_max) if row[age]!='' else age_mean/age_max
    row_dict[sex] = table_sex[row[sex]] if row[sex]!='' else table_sex['male']

    train_data.append(row_dict.values()) # adding each row to the data variable

#data_attributes = ['pclass', 'sibsp', 'parch', 'fare', 'embarked', 'age', 'sex']
data_attributes = [ names_columns_train[i] for i in row_dict.keys() ]

#: Then convert from a list to an array of float numbers !
train_data = np.array(train_data)#.astype(np.float)
train_data = shuffle(train_data) # Bizarre: semble pas changer grand chose, meme pour l'affichage.

# Building the test dataset
test_data = []
for row in test_file_object:
    row_dict = {}
    for attr in [pclass, sibsp, parch]:
     row_dict[attr] = int(row[attr-1])

    row_dict[fare] = float(row[fare-1])/float(fare_max) if row[fare-1]!='' else fare_mean/fare_max
    row_dict[embarked] = table_embarked[row[embarked-1]] if row[embarked-1]!='' else table_embarked['S']
    row_dict[age] = float(row[age-1])/float(age_max) if row[age-1]!='' else age_mean/age_max
    row_dict[sex] = table_sex[row[sex-1]] if row[sex-1]!='' else table_sex['male']

    test_data.append(row_dict.values())

#: Then convert from a list to an array of float numbers !
test_data = np.array(test_data)#.astype(np.float)

# Now the dataset is converted to floats and filled !
