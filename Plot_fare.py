#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset, regarding the fare.

:Source: `<../../Plot_fare.py>`_

Histogramme
-----------
Pour tracer les distributions de prix du ticket, on découpe en catégories.

.. image:: plots/passengers_bar50_fare.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_bar50_fare.*
   :align: center 


Histogramme (taux de survie)
----------------------------
Cela montre clairement que plus on paie cher plus on "a de change de survivre".

.. image:: plots/passengers_bar50_fare2.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_bar50_fare2.*
   :align: center


Diagramme
---------
Et ce dernier graphique le montre encore plus.

.. image:: plots/passengers_pie_fare.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_pie_fare.*
   :align: center
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics regarding the fare ...")
################################################################################
indexes_survived = data[0::, survived] == '1'
indexes_dead = data[0::, survived] == '0'

pylab.plot(data[indexes_survived, fare].astype(np.float), 'bo')
pylab.plot(data[indexes_dead, fare].astype(np.float), 'ro')
pylab.suptitle(u'Prix du ticket (en $)')
pylab.legend(('Survivants', 'Victimes'))
pylab.savefig("plots/passengers_scatter_fare.svg")
print("Ploting the passengers repartition on a scatter: plots/passengers_scatter_fare.svg")
pylab.draw()
pylab.clf()

################################################################################
for delta in [20, 50, 100]:
	x = [ i*delta for i in xrange(1+int( data[0::, fare].astype(np.float).max() / delta )) ]
	x2 = [ (i+0.5)*delta for i in xrange(len(x)) ]
	y0 = [ 0 for i in xrange(len(x)) ]
	y1 = [ 0 for i in xrange(len(x)) ]
	y2 = [ 0 for i in xrange(len(x)) ]

	for i in xrange(number_passengers):
	 y0[ int(data[i, fare].astype(np.float)) / delta ] += 1
	 if data[i, survived] == '1':
	  y1[ int(data[i, fare].astype(np.float)) / delta ] += 1
	 else:
	  y2[ int(data[i, fare].astype(np.float)) / delta ] += 1

	pylab.suptitle(u'Prix du ticket (en $), par tranche de %i.' % delta)
	
	pylab.subplot(2,1,1)
	pylab.bar(x, y0, color='black', edgecolor='white', width=delta-1)
	for i in xrange(len(x)):
	 if y0[i]>0:
	  pylab.text(x[i]+delta/2, y0[i]+0.05, '%.0f' % y0[i],
	   ha='center', va='bottom', fontsize='x-small')
	pylab.legend(['Passagers (total)'])
	pylab.ylabel(u"Nombre de passagers")

	pylab.subplot(2,1,2)
	pylab.bar(x, y1, color='blue', edgecolor='white', width=delta/2)
	pylab.bar(x2, y2, color='red', edgecolor='white', width=delta/2)
	pylab.legend(['Survivants', 'Victimes'])
	pylab.ylabel(u"Nombre de passagers")
	pylab.xlabel("Prix du ticket en $")
	for i in xrange(len(x)):
	 if y0[i]>0:
	  pylab.text(x[i]+delta/2, max(y1[i], y2[i])+0.05, 
	   '%2.2f%%' % (100.0*y1[i]/float(y0[i])),
	   ha='center', va='bottom', fontsize='x-small')

	pylab.savefig("plots/passengers_bar%i_fare.svg" % delta)
	print("Ploting the passengers repartition on a bar: plots/passengers_bar%i_fare.svg" % delta)
	pylab.draw()
	pylab.clf()

	################################################################################
	yn = [ 0 for i in xrange(len(x)) ]
	for i in xrange(len(yn)):
	 if y0[i]>0:
	   yn[i] = float(y1[i]) / float(y0[i])

	pylab.suptitle(u'Prix du ticket (en $), par tranche de %i.' % delta)

	pylab.subplot(2,1,1)
	pylab.bar(x, y0, color='black', edgecolor='white', width=delta-1)
	for i in xrange(len(x)):
	 if y0[i]>0:
	  pylab.text(x[i]+delta/2, y0[i]+0.05, '%.0f' % y0[i],
	   ha='center', va='bottom', fontsize='x-small')
	pylab.legend(['Passagers (total)'])
	pylab.ylabel(u"Nombre de passagers")

	pylab.subplot(2,1,2)
	pylab.bar(x, yn, color='green', edgecolor='white', width=delta-1)
	for i in xrange(len(x)):
	 if yn[i]>0:
	  pylab.text(x[i]+delta/2, yn[i]+0.005, '%.2f' % yn[i],
	   ha='center', va='bottom', fontsize='x-small')
	pylab.ylabel(u"Taux de survie")
	pylab.xlabel("Prix du ticket en $")

	pylab.savefig("plots/passengers_bar%i_fare2.svg" % delta)
	print("Ploting the passengers repartition on a bar: plots/passengers_bar%i_fare2.svg" % delta)
	pylab.draw()
	pylab.clf()

################################################################################
pylab.suptitle(u'Prix du ticket (en $)')

pylab.subplot(2,1,1)
pylab.pie(y0, autopct="%2.2f%%")
pylab.axis('equal')
pylab.title('Total')
pylab.legend( ['%s' % i for i in x] )

pylab.subplot(2,2,3)
pylab.pie(y1, autopct="%2.2f%%")
pylab.title('Survivants')
pylab.axis('equal')

pylab.subplot(2,2,4)
pylab.pie(y2, autopct="%2.2f%%")
pylab.title('Victimes')
pylab.axis('equal')

pylab.savefig("plots/passengers_pie_fare.svg")
print("Ploting the passengers repartition on a bar: plots/passengers_pie_fare.svg")
pylab.draw()
pylab.clf()
