#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset, regarding the pclass.

:Source: `<../../Plot_pclass.py>`_

Diagramme 1
-----------
Ce diagramme là montre la répartition des trois classes (1ere, 2nd, 3eme)
parmis les passagers, puis les survivants et les victimes.

.. image:: plots/passengers_pie_pclass1.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_pie_pclass1.*
   :align: center


Diagramme 2
-----------
Ce diagramme là montre la répartition des survivants et des victimes
parmis les trois classes (1ere, 2nd, 3eme).

.. image:: plots/passengers_pie_pclass2.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_pie_pclass2.*
   :align: center

Ce qui montre encore une fois que la première classe fut "plus sauvée" que les
deux autres.

Le taux de survie de la première est plus de deux fois supérieur que la troisième !
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics regarding the pclass ...")

################################################################################
indexes_1 = data[0::, pclass] == '1'
indexes_2 = data[0::, pclass] == '2'
indexes_3 = data[0::, pclass] == '3'

classes = [np.size(data[indexes_1, pclass]),
	np.size(data[indexes_2, pclass]),
	np.size(data[indexes_3, pclass])]


data_survived = data[ data[0::, survived] == '1', 0:: ]
indexes_1 = data_survived[0::, pclass] == '1'
indexes_2 = data_survived[0::, pclass] == '2'
indexes_3 = data_survived[0::, pclass] == '3'

classes_survived = [np.size(data_survived[indexes_1, pclass]),
	np.size(data_survived[indexes_2, pclass]),
	np.size(data_survived[indexes_3, pclass])]


data_dead = data[ data[0::, survived] == '0', 0:: ]
indexes_1 = data_dead[0::, pclass] == '1'
indexes_2 = data_dead[0::, pclass] == '2'
indexes_3 = data_dead[0::, pclass] == '3'

classes_dead = [np.size(data_dead[indexes_1, pclass]),
	np.size(data_dead[indexes_2, pclass]),
	np.size(data_dead[indexes_3, pclass])]

pylab.suptitle(u'Classe des passagers')

pylab.subplot(2,1,1)
pylab.pie(classes, autopct="%2.2f%%")
pylab.axis('scaled')
pylab.legend([
  u'1ère (%i)' % classes[0], 
  u'2nd (%i)' % classes[1], 
  u'3ième (%i)' % classes[2]
 ], loc='center', fontsize='xx-small')
pylab.title("Total (%i)" % number_passengers)

pylab.subplot(2,2,3)
pylab.pie(classes_survived, autopct="%2.2f%%")
pylab.axis('scaled')
pylab.legend([
  u'1ère (%i)' % classes_survived[0], 
  u'2nd (%i)' % classes_survived[1], 
  u'3ième (%i)' % classes_survived[2]
 ], loc='center', fontsize='xx-small')
pylab.title("Survivants (%i)" % number_survived)

pylab.subplot(2,2,4)
pylab.pie(classes_dead, autopct="%2.2f%%")
pylab.axis('scaled')
pylab.legend([
  u'1ère (%i)' % classes_dead[0], 
  u'2nd (%i)' % classes_dead[1], 
  u'3ième (%i)' % classes_dead[2]
 ], loc='center', fontsize='xx-small')
pylab.title("Victimes (%i)" % number_dead)

pylab.savefig("plots/passengers_pie_pclass1.svg")
print("Ploting the passengers repartition on a pie chart: plots/passengers_pie_pclass.svg")
pylab.draw()
pylab.clf()

################################################################################
data_1 = data[ data[0::, pclass] == '1', 0:: ]
data_2 = data[ data[0::, pclass] == '2', 0:: ]
data_3 = data[ data[0::, pclass] == '3', 0:: ]

classe_1 = [
	np.size(data_1[ data_1[0::, survived] == '0' , pclass]),
	np.size(data_1[ data_1[0::, survived] == '1' , pclass])]

classe_2 = [
	np.size(data_2[ data_2[0::, survived] == '0' , pclass]),
	np.size(data_2[ data_2[0::, survived] == '1' , pclass])]

classe_3 = [
	np.size(data_3[ data_3[0::, survived] == '0' , pclass]),
	np.size(data_3[ data_3[0::, survived] == '1' , pclass])]

pylab.suptitle(u'Proportion de survivants pour chaque classe')

pylab.subplot(2,1,1)
pylab.pie(classe_1, autopct="%2.2f%%", colors=['r', 'b'])
pylab.axis('scaled')
pylab.legend([
  u'Victimes (%i)' % classe_1[0], 
  u'Survivants (%i)' % classe_1[1]
 ], loc='center', fontsize='xx-small')
pylab.title(u'Première classe (%.0f)' % np.sum(classe_1))

pylab.subplot(2,2,3)
pylab.pie(classe_2, autopct="%2.2f%%", colors=['r', 'b'])
pylab.axis('scaled')
pylab.legend([
  u'Victimes (%i)' % classe_2[0], 
  u'Survivants (%i)' % classe_2[1]
 ], loc='center', fontsize='xx-small')
pylab.title(u'Seconde classe (%.0f)' % np.sum(classe_2))

pylab.subplot(2,2,4)
pylab.pie(classe_3, autopct="%2.2f%%", colors=['r', 'b'])
pylab.axis('scaled')
pylab.legend([
  u'Victimes (%i)' % classe_3[0], 
  u'Survivants (%i)' % classe_3[1]
 ], loc='center', fontsize='xx-small')
pylab.title(u'Troisième classe (%.0f)' % np.sum(classe_3))

pylab.savefig("plots/passengers_pie_pclass2.svg")
print("Ploting the passengers repartition on a pie chart: plots/passengers_pie_pclass2.svg")
pylab.draw()
pylab.clf()
