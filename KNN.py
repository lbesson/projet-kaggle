#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" A K Nearest Neighbors model.

Comme on doit utiliser une distance dans :math:`\mathbb{R}^d`
avec :math:`d = 8`, il faut que les données fournies soient
des nombres flottants, et soient complètes.

The doc is here : http://scikit-learn.org/dev/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

    $ python KNN.py

Résultats
---------
La soumission du résultat à Kaggle donne 71.70%.

--------------------------------------------------------------------------------
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from KaggleModel import *

################################################################################
# Beginning to learn
from sklearn.neighbors import KNeighborsClassifier
from sklearn.utils import shuffle

################################################################################
# ok, let use this 'cross validation' process to find the best
# meta parameter : n_neighbors
n_neighbors_quality = {}
list_n_neighbors = xrange(1,30)	#: Espace de recherche
Number_try = 10	#: Nombre de tests utilisés pour méta-apprendre
proportion_train = 0.65	#: Proportion d'individus utilisés pour méta-apprendre.
print("Find the best value for the meta parameter n_neighbors, with %i run for each..." % Number_try)
print("Searching in the range : %s..." % str(list_n_neighbors))

print("""Using the first part (%2.2f%%, %i passengers) of the training dataset as training, 
and the second part (%2.2f%%, %i passengers) as testing !"""
 % ( 100.0*proportion_train, int(number_passengers*proportion_train),
     100.0*(1-proportion_train), number_passengers - int(number_passengers*proportion_train) ))

for n_neighbors in list_n_neighbors:
#	train_data = shuffle(train_data)
	KNN = KNeighborsClassifier(n_neighbors = n_neighbors,
	 weights = 'distance') # weights = 'distance' or 'uniform'
	print("For %i Nearest Neighbors, learning from the first part of the dataset..." % n_neighbors)
	quality=[]
	for nb_essais in xrange(Number_try):
#		train_data = shuffle(train_data)
		KNN = KNN.fit(train_data[0:int(number_passengers*proportion_train),1::],
		 train_data[0:int(number_passengers*proportion_train),0])
		Output = KNN.predict(train_data[number_passengers - int(number_passengers*proportion_train)::,1::])
		quality.append(100.0 * Output[Output == train_data[number_passengers - int(number_passengers*proportion_train)::,0]].size / Output.size)
	n_neighbors_quality[n_neighbors] = np.mean(quality)
	print("... this value of n_neighbors seems to have a (mean) quality = %2.2f%%..." % np.mean(quality))

val = n_neighbors_quality.values()
#: La valeur optimale trouvée pour le paramètre n_estimators
best_n_neighbors = n_neighbors_quality.keys()[val.index(np.max(val))]
print("With trying each of the following n_neighbors (%s), each %i times, the best one is %.0f. (for a quality = %2.2f%%)"
 % (str(list_n_neighbors), Number_try, best_n_neighbors, np.max(val)))

################################################################################
# And then use this 'best' value of n_neighbors to predict on the test dataset
print("Creating the classifier with the optimal value of n_neighbors.")
KNN = KNeighborsClassifier(n_neighbors = best_n_neighbors,
	weights = 'distance') # weights = 'distance' or 'uniform'
print("Learning...")
KNN = KNN.fit(train_data[0::,1::],train_data[0::,0])
#: The score for this classifier.
score = (100.0*KNN.score(train_data[0::,1::], train_data[0::,0]) )
print(" Proportion of perfect fitting for the training dataset = %2.2f%%" % 
 score )
# ~ must be < 95%

# Predict on the testing set
test_file_object = csv.reader(open('test.csv', 'rb'))
header = test_file_object.next()

print("Predicting for the testing dataset")
Output = KNN.predict(test_data) 

# Write the output
open_file_object = csv.writer(open("csv/KNN_best.csv", "wb"))

z = 0
for row in test_file_object:
 row.insert(0, int(Output[z])) # Insert the prediction at the start of the row
 open_file_object.writerow(row) # Write the row to the file
 z += 1

print("Prediction: wrote in the file csv/KNN_best.csv.")

################################################################################
# Use feature selection to determine which feature is important.
import sklearn.feature_selection
#: chi2 statistics of each feature, p-values of each feature.
chi2, pval = sklearn.feature_selection.chi2(train_data[0::,1::], train_data[0::,0])

for i in xrange(len(chi2)):
	print("For the attribut %s	, chi2=%s	, and pval=%s." %
	 ( data_attributes[i], str(chi2[i]), str(pval[i]) ) )
