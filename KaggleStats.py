#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some statistics (printed on the screen) about the datas.

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

    $ python KaggleStats.py
"""

__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("OK, I read this file, I have now a huge array (of size : %s) !" % str(data.shape))

print("This array have %i passengers, and the following attributes :" % data[0::,0].size)
z = 0
for attributes in names_columns_train:
 print(" the %ith attribute is %s." % (z, attributes))
 print(" the values of this attribute are known for %2.2f%% of the passengers" %
  (100.0*data[ data[0::, z] != '' , 0].size / float(data[0::,0].size) ))
 z += 1

# Now I have an array of 11 columns and 891 rows
# I can access any element I want so the entire first column would
# be data[0::,0].astype(np.flaot) This means all of the columen and column 0
# I have to add the astype command
# as when reading in it thought it was a string so needed to convert

print("This array contains %.0f passengers..." % number_passengers)
print("and %.0f passengers survived the Titanic accident in 1912 :(" % number_survived)
print("(so %.0f passengers died)" % number_dead)
print("which gives a proportion of %.4f survivors." % proportion_survivors)


################################################################################
# I can now find the stats of all the women on board
print("\nConsidering the women and men ...")
print("The training data set contains %i women," % np.size(women_onboard))
print("The training data set contains %i men." % np.size(men_onboard))
print("Proportion of women who survived is %.4f," % proportion_women_survived)
print("Proportion of men who survived is %.4f." % proportion_men_survived)


################################################################################
# I can now find the stats for the three different embarcations points
print("""\nConsidering the three different embarcations points:
 * S = Southampton (EN) -> 1;
 * C = Cherbourg (FR) -> 0;
 * Q = Queenstown (EN) -> 2.
 """)
print("%i passengers took the Titanic at Southampton;" % np.size(from_s_onboard))
print("%i passengers took the Titanic at Cherbourg;" % np.size(from_c_onboard))
print("%i passengers took the Titanic at Queenstown.\n" % np.size(from_q_onboard))

print("Proportion of survivors, for the Southampton part, is %.4f;" % proportion_s_survived)
print("Proportion of survivors, for the Cherbourg part, is %.4f;" % proportion_c_survived)
print("Proportion of survivors, for the Queenstown part, is %.4f." % proportion_q_survived)


################################################################################
# I can now find the stats considering the age of the passengers
print("""\nConsidering the age of the passengers...""")
print("The age is known for %.2f%% of the passengers (so %i passengers)." % \
 (100.0*proportion_known_ages, np.size(known_ages[0::, survived])))

print("For them, the min age is %.2f." % age_min)
print("For them, the max age is %.2f." % age_max)
print("For them, the median age is %.2f." % age_mean)

print("The median age of the victims is %.3f." % known_ages_died[0::, age].astype(np.float).mean())
print("The median age of the survivors is %.3f." % known_ages_survived[0::, age].astype(np.float).mean())
print("""... so : no obvious link between age and survivance rate ?
Yeah, but maybe median age is not the best way to show it.""")


################################################################################
# I can now find the stats considering the fare of the passengers
print("""\nConsidering the fare of the passengers...""")
print("The fare is known for %.2f%% of the passengers (so %i passengers)." % \
 (100.0*proportion_known_fares, np.size(known_fares[0::, survived])))

print("For them, the min fare is %.2f." % fare_min)
print("For them, the max fare is %.2f." % fare_max)
print("For them, the median fare is %.2f." % fare_mean)

print("The median fare of the victims is %.3f." % known_fares_died[0::, fare].astype(np.float).mean())
print("The median fare of the survivors is %.3f." % known_fares_survived[0::, fare].astype(np.float).mean())

################################################################################
# Read the test dataset
print("Opening the file 'test.csv'...")

print("This array have %i passengers, and the following attributes :" % test_data[0::,0].size)
z = 0
for attributes in names_columns_test:
 print(" the %ith attribute is %s." % (z, attributes))
 print("   the values of this attribute are known for %2.2f%% of the passengers" %
  (100.0*test_data[ test_data[0::, z] != '' , 0].size / float(test_data[0::,0].size) ))
 z += 1

# END :)
