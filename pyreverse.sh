#!/bin/bash
#
#	This script is designed to be used with makePydoc, update__date__ and pytorst.
#	The reference page for this software is : 
#	https://sites.google.com/site/naereencorp/liste-des-projets/makepydoc
#
#	__author__='Lilian BESSON'
#	__email__='lilian.besson@normale.fr'
#	__version__='1.21'
#	__date__='dim. 17/02/2013 at 04h:25m:08s '
#
# Change the final *layout* of PyReverse graphs.

pyreverse -o dot -my -f ALL -p Kaggle [A-Z]*.py

add_shapes ()
{
 important="$1"
 shift
 cat packages_Kaggle.dot | sed s{"shape=\"box\", label=\"$important\""{"$* label=\"$important\""{ > packages_Kaggle.dot~
 cat packages_Kaggle.dot~ > packages_Kaggle.dot
}

for important in Kaggle KaggleModel KaggleStats Vote
do
add_shapes "$important" "shape=\"octagon\", color=\"yellow\", style=\"bold\""
done

for important in Plot_age Plot_fare Plot_gender Plot_parch Plot_passengers Plot_pclass Plot_port Plot_sibsp
do
add_shapes "$important" "color=\"blue\", style=\"bold\""
done

for important in AdaBoost DecisionTree DummyClassifier ExtraTrees GradientBoosting KNN RandomForest SVM NuSVC
do
add_shapes "$important" "color=\"green\", style=\"bold\""
done

for important in RNN
do
add_shapes "$important" "color=\"red\", style=\"bold\""
done

for important in conf
do
add_shapes "$important" "shape=\"triangle\", style=\"bold\""
done

dot -Tsvg packages_Kaggle.dot > packages_Kaggle.svg
#dot -Tsvg classes_Kaggle.dot > classes_Kaggle.svg

dot -Tpng packages_Kaggle.dot > packages_Kaggle.png
#dot -Tpng classes_Kaggle.dot > classes_Kaggle.png

dot -Tpdf packages_Kaggle.dot > packages_Kaggle.pdf
#dot -Tpdf classes_Kaggle.dot > classes_Kaggle.pdf

mv packages_Kaggle.* classes_Kaggle.* images/
