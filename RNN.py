#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" A Radius Nearest Neighbors model.

The doc is here : http://scikit-learn.org/dev/modules/generated/sklearn.neighbors.RadiusNeighborsClassifier.html#sklearn.neighbors.RadiusNeighborsClassifier

Limitation
----------
Le rayon doit être grand pour que chaque point ait au moins un voisin,
sinon l'algorithme râle !

À propos
--------
Dans ce script, j'utilise sklearn.grid_search pour explorer automatiquement
un ensemble de méta-paramètre.

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

    $ python RNN.py

--------------------------------------------------------------------------------
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from KaggleModel import *
from sklearn.neighbors import RadiusNeighborsClassifier
from sklearn.grid_search import ParameterGrid

################################################################################
list_radius = [1.0*10**i for i in xrange(2,5)] + [5.0*10**i for i in xrange(2,5)]
param_grid = {'radius':list_radius, 'leaf_size':range(1,20,1)}

# Exhaustive search over specified parameter values for an estimator.
print("Searching for parameters in %s." % param_grid)
import sklearn.grid_search
clf = sklearn.grid_search.GridSearchCV( RadiusNeighborsClassifier(), param_grid )

################################################################################
print("Learning...")
clf = clf.fit(train_data[0::,1::],train_data[0::,0])
#: The score for this classifier.
score = (100.0*clf.score(train_data[0::,1::], train_data[0::,0]) )
print(" Proportion of perfect fitting for the training dataset = %2.2f%%" % 
 score )

print("The best parameters are : %s." % clf.best_params_)

# Predict on the testing set
test_file_object = csv.reader(open('test.csv', 'rb'))
header = test_file_object.next()

print("Predicting for the testing dataset")
Output = clf.predict(test_data) 
open_file_object = csv.writer(open("csv/RNN_best.csv", "wb"))

z = 0
for row in test_file_object:
 row.insert(0, int(Output[z])) # Insert the prediction at the start of the row
 open_file_object.writerow(row) # Write the row to the file
 z += 1

print("Prediction: wrote in the file csv/RNN_best.csv.")
