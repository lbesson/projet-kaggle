Kaggle Machine Learning project
===============================

This project is my proposal to the [Titanic project](https://www.kaggle.com/c/titanic-gettingStarted),
proposed on Kaggle.com, and it will be my ML project for the ML lesson, at ENS de Cachan.

----

### Author:
Lilian Besson (*for more details, see the AUTHOR file in the source*).

### Language:
 * Python v2.7.3
   with :
    * matplotlib,
    * numpy,
    * sklearn,
    * pylab,

Documentation
-------------

The doc can be consulted on one of this page :

* on **ENS de Cachan** website : [publis/kaggle/.build/html/](http://www.dptinfo.ens-cachan.fr/~lbesson/publis/kaggle/.build/html/ "Hosted by the dpt info of 'ENS de Cachan'").
* on **Cr@ns** website [publis/kaggle/.build/html/](http://perso.crans.org/besson/publis/kaggle/.build/html/ "Hosted by the 'Cr@ns' association").
 
**All the details (installation, options, etc) are in the doc**.
Anyway, here are somes infos.

----

Installation
============

Dependencies
------------
 
The project is *entirely written in Python*, version *2.7.3*.

For more details about the **Python** language, see [the official site](http://www.python.org> "Python power !").
Python 2.7.1 or higher is **required**.

The project also **require** the following *unusual module(s)* :
 * matplotlib,
 * numpy,
 * sklearn,
 * pylab.

Plateform(s)
------------

The project have been *developped* on *GNU/Linux* (Ubuntu 11.10).

#### Warning (Windows)
Not tested.

#### Warning (Mac OS X)  
Not tested.

What is the important part ?
----------------------------

## plots/

Contains some graphics, showing some stuff about the datas.

## csv/

Contains some previsions, not yet rated for some of them.

----

About the project
=================

This project was realised for the L3 **machine learning lesson**, at ENS de Cachan.

About the doc
=============
 
The documentation is produced mainly with **Sphinx**, the Python's documentation generator.
 
I wrote a few scripts to help *Sphinx* to do what I wanted, and to generate a *PyDoc* version of the doc too.
 
Those scripts constitute now an independant and a powerful project.
It is hosted [here on my Google Site](https://sites.google.com/site/naereencorp/liste-des-projets/makepydoc "check this out too ;) !")


Contact me
----------

Feel free to contact me, either with a bitbucket message (my profile is [lbesson](https://bitbucket.org/lbesson/ "here")), or via an email at **lilian DOT besson AT ens-cachan DOT fr**.

License
-------

This project is released under the **GPLv3 license**, for more details, take a look at the LICENSE file in the source.
*Basically, that allow you to use all or part of the project for you own business.*
