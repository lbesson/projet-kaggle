#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset.

:Source: `<../../Plot_passengers.py>`_

Diagramme
---------
Ce diagramme là montre la répartition des survivants et des victimes
parmis les passagers.

.. image:: plots/passengers_chart.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_chart.*
   :align: center


Diagramme (genre)
-----------------
Ce diagramme là montre la répartition des survivants et des victimes
parmis les hommes et les femmes.

.. image:: plots/passengers_chart_gender.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_chart_gender.*
   :align: center

Ce diagramme montre aussi que les femmes survivèrent bien plus que les hommes.
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics ...")
################################################################################
# I want now to plot some graphics about the datas, with matplotlib
# Histogram
X = np.arange(3)
Y = np.array([number_passengers, number_dead, number_survived])
pylab.bar(X, +Y, color='#ff9999', edgecolor='white')

pylab.text(X[0]+0.4, Y[0]+0.05, 'Passagers', ha='center', va='bottom')
pylab.text(X[1]+0.4, Y[1]+0.05, 'Victimes', ha='center', va='bottom')
pylab.text(X[2]+0.4, Y[2]+0.05, 'Survivants', ha='center', va='bottom')

pylab.suptitle(u"Répartitions des passagers du Titanic")
pylab.savefig("plots/passengers_hist.svg") #, dpi=256)
print("Ploting the passengers repartition on an histogram: plots/passengers_hist.svg")
pylab.draw()
pylab.clf()

# Pie chart
pylab.suptitle(u"Répartitions des passagers du Titanic")
pylab.pie([number_dead, number_survived],\
 labels=["Victimes (%.0f)" % number_dead,\
 "Survivants (%.0f)" % number_survived],\
 autopct="%.2f%%", colors=['r','b'])
pylab.axis('scaled')
pylab.legend(['Victimes', 'Survivants'], loc='center', fontsize='x-small')
pylab.savefig("plots/passengers_chart.svg")
print("Ploting the passengers repartition on a pie chart: plots/passengers_chart.svg")
pylab.draw()
pylab.clf()

# The same pie chart, but for women and men
pylab.suptitle(u"Répartitions des passagers du Titanic (femmes et hommes)")

pylab.subplot(1, 2, 1)
pylab.title("Femmes (%i)" % np.size(women_onboard))
pylab.pie([1.0-proportion_women_survived, proportion_women_survived],\
 autopct="%.2f%%", colors=['r','b'])
pylab.axis('scaled')
pylab.legend(['Victimes', 'Survivants'], loc='center', fontsize='x-small')

pylab.subplot(1, 2, 2)
pylab.title("Hommes (%i)" % np.size(men_onboard))
pylab.pie([1.0-proportion_men_survived, proportion_men_survived],\
 autopct="%.2f%%", colors=['r','b'])
pylab.axis('scaled')
pylab.legend(['Victimes', 'Survivants'], loc='center', fontsize='x-small')

pylab.savefig("plots/passengers_chart_gender.svg")
print("Ploting the passengers repartition according to their gender on a pie chart: plots/passengers_chart.svg")
pylab.draw()
pylab.clf()
