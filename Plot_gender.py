#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset, regarding the gender.

:Source: `<../../Plot_gender.py>`_

Histogramme
-----------
Cela montre la distribution des survivants et les victimes
selon le genre des passagers.

.. image:: plots/passengers_hist_gender.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_hist_gender.*
   :align: center 


Diagramme
---------
Clairement, cela montre encore qu'on sauvait plus les femmes que les hommes
en 1912 !

.. image:: plots/passengers_chart_gender2.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_chart_gender2.*
   :align: center
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics regarding the gender ...")

################################################################################

# Histogram for the embarcation ports
X = np.arange(3)
Y1 = np.array([number_passengers, np.size(women_onboard), np.size(men_onboard)])
Y2 = np.array([number_dead, np.sum(women_onboard), np.sum(men_onboard)])
pylab.bar(X, +Y1, color='#ff9999', edgecolor='white')
pylab.bar(X+0.1, +Y2, color='#99ff99', edgecolor='white')

pylab.text(X[0]+0.4, Y1[0]+0.05, 'Total',\
 ha='center', va='bottom', fontsize='x-small')
pylab.text(X[1]+0.4, Y1[1]+0.05, u'Femmes (%2.2f%%)' % (100.0*proportion_women), \
 ha='center', va='bottom', fontsize='x-small')
pylab.text(X[2]+0.4, Y1[2]+0.05, 'Hommes (%2.2f%%)' % (100.0*proportion_men), \
 ha='center', va='bottom', fontsize='x-small')
 
pylab.text(X[0]+0.4, Y2[0]+0.05, 'Survivants',\
 ha='center', va='bottom', fontsize='x-small')
pylab.text(X[1]+0.4, Y2[1]+0.05, 'Survivants F (%2.2f%%)' % (100.0*proportion_women_survived), \
 ha='center', va='bottom', fontsize='x-small')
pylab.text(X[2]+0.4, Y2[2]+0.05, 'Survivants M (%2.2f%%)' % (100.0*proportion_men_survived), \
 ha='center', va='bottom', fontsize='x-small')

pylab.legend(['# passagers', '# survivants'], loc='center')

pylab.suptitle(u"Répartitions des passagers du Titanic (selon le genre)")
pylab.savefig("plots/passengers_hist_gender.svg")
print("Ploting the passengers repartition on an histogram: plots/passengers_hist_gender.svg")
pylab.draw()
pylab.clf()

# The same, but on a pie chart
pylab.suptitle(u"Répartitions des passagers du Titanic (selon le genre)")

pylab.subplot(2, 1, 1)
pylab.title("Passagers (%i)" % number_passengers)
pylab.pie(Y1[1::],autopct="%.2f%%", colors=['r','b'])
pylab.axis('scaled')
pylab.legend(['Femmes', 'Hommes'], fontsize='xx-small')

pylab.subplot(2, 2, 3)
pylab.title("Victimes (%i)" % number_dead)
pylab.pie(Y1[1::]-Y2[1::],autopct="%.2f%%", colors=['r','b'])
pylab.axis('scaled')
pylab.legend(['Femmes', 'Hommes'], fontsize='xx-small')

pylab.subplot(2, 2, 4)
pylab.title("Survivants (%i)" % number_survived)
pylab.pie(Y2[1::],autopct="%.2f%%", colors=['r','b'])
pylab.axis('scaled')
pylab.legend(['Femmes', 'Hommes'], fontsize='xx-small')

pylab.savefig("plots/passengers_chart_gender2.svg")
print("Ploting the passengers repartition on an histogram: plots/passengers_chart_gender2.svg")
pylab.draw()
pylab.clf()
