#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset, regarding the embarcation port.

:Source: `<../../Plot_port.py>`_

Histogramme
-----------
Cet histogramme montre répartition des survivants et des victimes
parmis les trois ports d'embarcations (Southampton, Cherbourg, Queenstown).

Clairement, les deux derniers ports d'embarcations ont embarqués bien moins
de passagers que Southampton.
Donc, il faut nuancer les remarques suivantes, puisqu'il y a peu de passagers
venant de Cherbourg et de Queenstown.

.. image:: plots/passengers_hist_port.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_hist_port.*
   :align: center


Diagramme
---------
Ce diagramme là montre la répartition des trois ports d'embarcations
(Southampton, Cherbourg, Queenstown) parmis les survivants et des victimes
(soit le point de vue inverse).

.. image:: plots/passengers_chart_port.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_chart_port.*
   :align: center


Ces deux graphiques montrent que les passagers ayant embarqués à Southampton
semblent plus "fragiles" alors que ceux ayant embarqués à Cherbourg semblent
plus "robustes".
Et pour Queenstown (les Irlandais), il ne semble pas y avoir de différence.
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics (2) ...")

################################################################################
# I want now to plot some graphics about the datas, with matplotlib

# Histogram for the embarcation ports
X = np.arange(3)
Y1 = np.array([np.size(from_s_onboard), np.size(from_c_onboard), np.size(from_q_onboard)])
Y2 = np.array([np.sum(from_s_onboard), np.sum(from_c_onboard), np.sum(from_q_onboard)])
pylab.bar(X, +Y1, color='#ff9999', edgecolor='white')
pylab.bar(X+0.1, +Y2, color='#99ff99', edgecolor='white')

pylab.text(X[0]+0.4, Y1[0]+0.05, 'Southampton (%2.2f%%)' % (100.0*proportion_s_survived),\
 ha='center', va='bottom', fontsize='x-small')
pylab.text(X[1]+0.4, Y1[1]+0.05, 'Cherbourg (%2.2f%%)' % (100.0*proportion_c_survived), \
 ha='center', va='bottom', fontsize='x-small')
pylab.text(X[2]+0.4, Y1[2]+0.05, 'Queenstown (%2.2f%%)' % (100.0*proportion_q_survived), \
 ha='center', va='bottom', fontsize='x-small')

pylab.legend(['Total', '# survivants'], loc='center')

pylab.suptitle(u"Répartitions des passagers du Titanic (selon le port d'embarcation)")
pylab.savefig("plots/passengers_hist_port.svg")
print("Ploting the passengers repartition on an histogram: plots/passengers_hist_port.svg")
pylab.draw()
pylab.clf()

# The same, but on a pie chart
pylab.suptitle(u"Répartitions des passagers du Titanic (selon le port d'embarcation)")

pylab.subplot(2, 1, 1)
pylab.title("Passagers (%i)" % number_passengers)
pylab.pie(Y1,autopct="%.2f%%", colors=['r','b','g'])
pylab.axis('scaled')
pylab.legend(['Southampton', 'Cherbourg', 'Queenstown'], fontsize='xx-small')

pylab.subplot(2, 2, 3)
pylab.title("Victimes (%i)" % number_dead)
pylab.pie(Y1-Y2,autopct="%.2f%%", colors=['r','b','g'])
pylab.axis('scaled')
pylab.legend(['Southampton', 'Cherbourg', 'Queenstown'], fontsize='xx-small')

pylab.subplot(2, 2, 4)
pylab.title("Survivants (%i)" % number_survived)
pylab.pie(Y2,autopct="%.2f%%", colors=['r','b','g'])
pylab.axis('scaled')
pylab.legend(['Southampton', 'Cherbourg', 'Queenstown'], fontsize='xx-small')

pylab.savefig("plots/passengers_chart_port.svg")
print("Ploting the passengers repartition on an histogram: plots/passengers_chart_port.svg")
pylab.draw()
pylab.clf()
