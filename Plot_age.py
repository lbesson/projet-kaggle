#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Some graphics about the training dataset, regarding the age.

:Source: `<../../Plot_age.py>`_

Histogramme
-----------
Pour tracer les distributions d'âge, on découpe en catégories.
Pour ce graphique, par tranche de 10 ans.

.. image:: plots/passengers_bar_age10.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_bar_age10.*
   :align: center 


Histogramme (taux de survie)
----------------------------
Ça montre notamment qu'en 1912 on appliquait à la lettre les principes
*"sauvez les enfants et les personnes agées d'abord !"*.

.. image:: plots/passengers_bar2_age10.*
   :height: 600px
   :width: 1100 px
   :alt: ../../plots/passengers_bar2_age10.*
   :align: center
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from Kaggle import *

print("\n Ploting some graphics regarding the age ...")

################################################################################
indexes_survived = known_ages[0::, survived] == '1'
indexes_dead = known_ages[0::, survived] == '0'

pylab.plot(known_ages[indexes_survived, age], 'bo')
pylab.plot(known_ages[indexes_dead, age], 'ro')
pylab.suptitle(u'Age des passagers')
pylab.legend(('Survivants', 'Victimes'))
pylab.savefig("plots/passengers_scatter_age.svg")
print("Ploting the passengers repartition on a scatter: plots/passengers_scatter_age.svg")
pylab.draw()
pylab.clf()

################################################################################
for delta_age in [1, 2, 5, 8, 10, 100]:
	x = [ i*delta_age for i in xrange(1+int(age_max/delta_age)) ]
	y0 = [ 0.0 for i in xrange(len(x)) ]
	y1 = [ 0.0 for i in xrange(len(x)) ]
	y2 = [ 0.0 for i in xrange(len(x)) ]
	for i in xrange(np.size(known_ages[0::, survived])):
	 y0[ int(known_ages[i, age].astype(np.float) / delta_age) ] += 1
	 if known_ages[i, survived] == '1':
	  y1[ int(known_ages[i, age].astype(np.float) / delta_age) ] += 1
	 else:
	  y2[ int(known_ages[i, age].astype(np.float) / delta_age) ] += 1

	pylab.suptitle(u'Age des passagers')

	pylab.subplot(3,1,1)
	pylab.bar(x, y0, color='black', edgecolor='white', width=delta_age-0.3)
	pylab.legend(['Passagers (total)'])
	pylab.ylabel(u"Nombre de passagers")

	pylab.subplot(3,1,2)
	pylab.bar(x, y1, color='blue', edgecolor='white', width=delta_age-0.3)
	pylab.legend(['Survivants'])
	
	pylab.subplot(3,1,3)
	pylab.bar(x, y2, color='red', edgecolor='white', width=delta_age-0.3)
	pylab.legend(['Victimes'])
	
	pylab.xlabel(u"Tranches d'âges de %i an(s)" % delta_age)
	pylab.ylabel(u"Nombre de passagers")

	pylab.savefig("plots/passengers_bar_age%i.svg" % delta_age)
	print("Ploting the passengers repartition on a bar: plots/passengers_bar_age%i.svg" % delta_age)
	pylab.draw()
	pylab.clf()

################################################################################
for delta_age in [1, 2, 5, 8, 10, 100]:
	x = [ i*delta_age for i in xrange(1+int(age_max/delta_age)) ]
	x1 = [ i*delta_age + 0.75 for i in xrange(1+int(age_max/delta_age)) ]
	y0 = [ 0.0 for i in xrange(len(x)) ]
	y1 = [ 0.0 for i in xrange(len(x)) ]
	y2 = [ 0.0 for i in xrange(len(x)) ]
	for i in xrange(np.size(known_ages[0::, survived])):
	 y0[ int(known_ages[i, age].astype(np.float) / delta_age) ] += 1
	 if known_ages[i, survived] == '1':
	  y1[ int(known_ages[i, age].astype(np.float) / delta_age) ] += 1
	 else:
	  y2[ int(known_ages[i, age].astype(np.float) / delta_age) ] += 1
	for i in xrange(len(x)):
	 if y0[i]>0:
	   y1[i] /= y0[i]
	   y2[i] /= y0[i]

	pylab.suptitle(u'Age des passagers')

	pylab.subplot(2,1,1)
	pylab.bar(x, y0, color='black', edgecolor='white', width=delta_age-0.3)
	pylab.legend(['Passagers (total)'])
	pylab.ylabel(u"Nombre de passagers")

	pylab.subplot(2,1,2)
	pylab.bar(x, y1, color='green', edgecolor='white', width=delta_age-0.6)
	
	pylab.xlabel(u"Tranches d'âges de %i an(s)" % delta_age)
	pylab.ylabel(u"Taux de survie")

	pylab.savefig("plots/passengers_bar2_age%i.svg" % delta_age)
	print("Ploting the survival rate on a bar: plots/passengers_bar2_age%i.svg" % delta_age)
	pylab.draw()
	pylab.clf()
