#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" A representation tool for Titanic dataset.

The doc is here : http://scikit-learn.org/dev/modules/classes.html

--------------------------------------------------------------------------------

Je veux essayer d'utiliser les algorithmiques de clustering, de changement
de représentations etc...

Sortie du script
----------------
.. runblock:: console

    $ python Representation.py

--------------------------------------------------------------------------------
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from KaggleModel import *
train_data = shuffle(train_data)

################################################################################
import sklearn

################################################################################
# Preprocessing: is useless.
#import sklearn.preprocessing
#train_data = sklearn.preprocessing.scale(train_data)

################################################################################
# Use feature selection to determine which feature is important.
import sklearn.feature_selection

#: chi2 statistics of each feature, p-values of each feature.
chi2, pval = sklearn.feature_selection.chi2(train_data[0::,1::], train_data[0::,0])
print("chi2:")
for i in xrange(len(chi2)):
	print("	For the attribute %s	, chi2=%s	, and pval=%s." %
	 ( data_attributes[1+i], str(chi2[i]), str(pval[i]) ) )
	 
# http://scikit-learn.org/dev/modules/generated/sklearn.feature_selection.f_regression.html#sklearn.feature_selection.f_regression
#: Quick linear model for testing the effect of a single regressor, sequentially for many regressors.
F, pval = sklearn.feature_selection.f_regression(train_data[0::,1::], train_data[0::,0], center=False)
print("f_regression:")
for i in xrange(len(F)):
	print("	For the attribute %s	, F=%s	, and pval=%s." %
	 ( data_attributes[1+i], str(F[i]), str(pval[i]) ) )
	 
# http://scikit-learn.org/dev/modules/generated/sklearn.feature_selection.f_classif.html#sklearn.feature_selection.f_classif
#: Quick linear model for testing the effect of a single regressor, sequentially for many regressors.
F, pval = sklearn.feature_selection.f_classif(train_data[0::,1::], train_data[0::,0])
print("f_classif:")
for i in xrange(len(F)):
	print("	For the attribute %s	, F=%s	, and pval=%s." %
	 ( data_attributes[1+i], str(F[i]), str(pval[i]) ) )

################################################################################
# Try each others tools provided by sklearn ?

# Covariance
# http://scikit-learn.org/dev/modules/generated/sklearn.covariance.empirical_covariance.html#sklearn.covariance.empirical_covariance
import sklearn.covariance
cov = sklearn.covariance.empirical_covariance(train_data[0::,1::])

# Principal component analysis (PCA)
# http://scikit-learn.org/dev/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA
import sklearn.decomposition
# Sparse Random Projection
# http://scikit-learn.org/dev/modules/generated/sklearn.random_projection.SparseRandomProjection.html#sklearn.random_projection.SparseRandomProjection
import sklearn.random_projection
# Gaussian Random Projection
# http://scikit-learn.org/dev/modules/generated/sklearn.random_projection.GaussianRandomProjection.html#sklearn.random_projection.GaussianRandomProjection
import random

#for n_components in xrange(1, train_data.shape[1]):
#	print("\nPCA decomposition with %i n_components." % n_components)
#	clf = sklearn.decomposition.PCA(n_components = n_components)
##	clf = sklearn.random_projection.SparseRandomProjection(n_components = n_components)
##	clf = sklearn.random_projection.GaussianRandomProjection(n_components = n_components, random_state=0)
#	clf = clf.fit(train_data[0::,1::])
#	 Percentage of variance explained by each of the selected components.
#	 k is not set then all components are stored and the sum of explained variances is equal to 1.0
#	print("Percentage of variance explained by each of the selected components = %s." % clf.explained_variance_ratio_)
#	X_new = shuffle( clf.transform(train_data[0::, 1::]) )
#	 Should allow plotting in 2D, with 2 colors for victims and survivors.
#	indexes_survived = train_data[0::, survived] == 1
#	indexes_dead = train_data[0::, survived] == 0

#	if n_components > 1:
#		z = range(n_components)
#		x = random.choice(z)
#		z.remove(x)
#		y = random.choice(z)
#		print("For the plotting: %i -> x; %i -> y." % (x,y))
#		
#		pylab.plot(X_new[indexes_survived][:,x], X_new[indexes_survived][:,y], 'b.')
#		pylab.plot(X_new[indexes_dead][:,x], X_new[indexes_dead][:,y], 'r.')
#		pylab.xlabel(u"%ith new feature" % x)
#		pylab.ylabel(u"%ith new feature" % y)
#	else:
#		pylab.plot(X_new[indexes_survived], 'b.')
#		pylab.plot(X_new[indexes_dead], 'r.')

#	pylab.suptitle(u'Representation with GaussianRandom, n_components=%i' % n_components)
#	pylab.legend(('Survivants', 'Victimes'))
#	pylab.savefig("plots/Representation_GaussianRandom%i.svg" % n_components, dpi=200)
#	print("Ploting the passengers repartition on a scatter: plots/Representation_GaussianRandom%i.svg" % n_components)
#	pylab.draw()
#	pylab.clf()

#	print X_new[0,0::]

################################################################################
# Plotting, without PCA
#indexes_survived = train_data[0::, survived] == 1
#indexes_dead = train_data[0::, survived] == 0

#print data_attributes

#for x in xrange(0,train_data.shape[1]):
# for y in range(0,x)+range(x+1,train_data.shape[1]):
#	print("For the plotting: %i -> x = %s; %i -> y = %s."
#	 % (x, data_attributes[x], y, data_attributes[y]) )

#	pylab.plot(train_data[indexes_survived][:,x], train_data[indexes_survived][:,y], 'b+')
#	pylab.plot(train_data[indexes_dead][:,x], train_data[indexes_dead][:,y], 'rx')
#	pylab.xlabel(u"%ith feature (%s)" % (x, data_attributes[x]) )
#	pylab.ylabel(u"%ith feature (%s)" % (y, data_attributes[y]) )

#	pylab.suptitle(u'Representation with choosing 2 features (%s=f(%s)).'
#	 % (data_attributes[y], data_attributes[x]))
#	pylab.legend(('Survivants', 'Victimes'))
#	pylab.savefig("plots/Representation_x%i_y%i.svg" % (x,y), dpi=200)
#	print("Ploting the passengers repartition on a scatter: plots/Representation_x%i_y%i.svg" % (x,y))
#	pylab.draw()
#	pylab.clf()


################################################################################
# 

