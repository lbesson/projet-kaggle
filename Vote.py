#!/usr/bin/env python
# -*- encoding: utf-8 -*-
""" Un voteur.

Ce script lit toutes les prédictions déjà réalisées,
et fait un simple vote pour tenter de les améliorer !

--------------------------------------------------------------------------------

Sortie du script
----------------
.. runblock:: console

    $ python Vote.py

Résultats
---------
La soumission du résultat à Kaggle donne ??.??%.

--------------------------------------------------------------------------------
"""
__author__	= 'Lilian BESSON (mailto:lilian.besson@normale.fr)'

from KaggleModel import *
import sys, os

################################################################################
# Reading the predictions
csv_files = os.listdir("csv/")
# Remove the hiding ones
csv_files = [filename for filename in csv_files if filename[0] != '.']
# Remove the vote one
csv_files = [filename for filename in csv_files if filename != 'Vote_best.csv']
# Keep the CSV files
csv_files = [filename for filename in csv_files if filename[-4:] == '.csv'] #: Fichiers utilisés pour le vote.
print("Voting, using the files %s." % str(csv_files))

################################################################################
Outputs = {}
# Read the predictions
for filename in csv_files:
	print("For the file %s, re-constructing the prediction." % filename)
	prediction_file_object = csv.reader(open('csv/'+filename, 'rb'))
	Outputs[filename] = []
	for row in prediction_file_object:
	 Outputs[filename].append(int( row[0] ))
	print("\tthe prediction said %2.2f%% of victims..." %
	 (float(sum(Outputs[filename])) / len(Outputs[filename])) )

def vote(Outputs, z=0):
	"""vote(Outputs, z=0) -> {0|1}

Make the vote, by regarding the most probable output,
by considering all the predictions, stored in Outputs :

 * Outputs must be a dictionnary {filename:Output}.

 * And Output is a prediction int-array (like [1,1,1,1,0,0,1,0,1,1,0,...]).

 * z is the index of the passenger.
	"""
	out = []
	for filename in csv_files:
		out.append( Outputs[filename][z] )
	print("  for the %ith passenger, the vote of all predictions is %s." %
	 (z, str(np.mean(out))) )
	if np.mean(out) <= 0.5:
	 return 0
	else:
	 return 1

################################################################################
# Predict on the testing set
test_file_object = csv.reader(open('test.csv', 'rb'))
header = test_file_object.next()

print("Predicting for the testing dataset")
# Write the output
open_file_object = csv.writer(open("csv/Vote_best.csv", "wb"))

z = 0
for row in test_file_object:
 row.insert(0, vote( Outputs, z) ) # Insert the prediction at the start of the row
 open_file_object.writerow(row) # Write the row to the file
 z += 1

print("Prediction: wrote in the file csv/Vote_best.csv.")
